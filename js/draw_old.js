const STEP = 15;

var ModelUtil = {};





var paper;
var masterBackground;
var ctrl = false;
var shift = false;

Raphael.el.is_visible = function() {
    return (this.node.style.display !== "none");
}

Raphael.st.draggable = function() {
  var me = this,
      lx = 0,
      ly = 0,
      ox = 0,
      oy = 0,
      moveFnc = function(dx, dy) {
          lx = dx + ox;
          ly = dy + oy;
          me.transform('t' + lx + ',' + ly);
      },
      startFnc = function() {},
      endFnc = function() {
          ox = lx;
          oy = ly;
      };
 
  this.drag(moveFnc, startFnc, endFnc);
}

document.onkeydown = function(event) {
    //console.log(event);
    if (event.keyCode === 17) {
        ctrl = true;
    }
    if (event.keyCode === 16) {
        shift = true;
        $("#draw_container").css({cursor:'crosshair'});
    }
};
document.onkeyup = function(event) {
    //console.log(event);
    if (event.keyCode === 17) {
        ctrl = false;
    }
    if (event.keyCode === 16) {
        shift = false;
        $("#draw_container").css({cursor:'default'});
    }
};

window.onload = function() {

    //paper = Raphael("draw_container", draw_container.offsetWidth, draw_container.offsetHeight);
    paper = Raphael("draw_area", draw_area.offsetWidth, draw_area.offsetWidth);

    console.log($("#draw_container"));

    masterBackground = paper.rect(0,0, 3000, 3000);
    masterBackground.attr("fill", "#fff");
    masterBackground.attr("fill-opacity", 0.2);

    var paperStart = function(event) {
        //console.log("start");
        $("#draw_container").data("scrolTopVal",$("#draw_container").scrollTop());
        $("#draw_container").data("scrolLeftVal",$("#draw_container").scrollLeft());
        if(masterBackground.is_visible()){
            $("#draw_container").css({cursor:'move'});           
        }


        // console.log($("#draw_container").data());
    }

    var paperMove = function(dx, dy) {
        // console.log("move");
        $("#draw_container").scrollTop($("#draw_container").data("scrolTopVal")-dy);
        $("#draw_container").scrollLeft($("#draw_container").data("scrolLeftVal")-dx);
    }  

    var paperEnd = function() {
        $("#draw_container").css({cursor:'default'});
    }
    masterBackground.drag(paperMove, paperStart, paperEnd);
}


function add_Set() {
    var mySet = paper.set();
    mySet.push(paper.circle(150, 150, 110).attr('fill', 'red')); 
    mySet.push(paper.circle(150, 150, 100).attr('fill', 'white'));
    mySet.push(paper.circle(150, 150, 90).attr('fill', 'red'));
    mySet.push(paper.circle(150, 150, 80).attr('fill', 'white'));
    mySet.push(paper.circle(150, 150, 70).attr('fill', 'red'));
    mySet.push(paper.circle(150, 150, 60).attr('fill', 'white'));
    mySet.push(paper.circle(150, 150, 50).attr('fill', 'red'));
    mySet.push(paper.circle(150, 150, 40).attr('fill', 'white'));
    mySet.push(paper.circle(150, 150, 30).attr('fill', 'red'));
    mySet.push(paper.circle(150, 150, 20).attr('fill', 'white'));
    mySet.push(paper.circle(150, 150, 10).attr('fill', 'red'));
     
    mySet.draggable();
    mySet.click(function(){
        console.log("dfrgsert");
    });
}



function draw_path() {
    var pathStr;
    var path;
    var pX;
    var pY;
    var mode = 'add';
    var storePos = [];
    var storePosSelectionBlock = [];
    var minCord = {};

    masterBackground.hide();

    var clickfn = function() {
        console.log("click_______");
        this.bringToFront();
        this.selectionBlock.show();
        //ModelUtil.selectNode(this);
    }
    var add_child = function(child) 
    { 
        path.children.push(child); 
        child.mainnode = path; 
        //child.hover(hoverfn, hoverOutfn);
        child.click(clickfn);
        return child;
    };

    var pathDragStart = function () {
        masterBackground.hide();   
        $("#draw_container").css({cursor:'move'});
        pathStr = path.attr('path');
        storePosPath = [];
        storePosSelectionBlock = [];
        for (var i = 0; i < pathStr.length-1; i++) {
            storePosPath.push([pathStr[i][1], pathStr[i][2]]);
        };

        storePosSelectionBlock.push(path.selectionBlock.attr("x"));
        storePosSelectionBlock.push(path.selectionBlock.attr("y"));
    }; 

    var pathDragMove = function (dx, dy) { 
        pathStr = path.attr('path');
        for (var i = 0; i < storePosPath.length; i++) {
            pathStr[i][1] = storePosPath[i][0] + dx;
            pathStr[i][2] = storePosPath[i][1] + dy;
        };
        path.attr('path', pathStr);

        path.selectionBlock.attr({
            x: storePosSelectionBlock[0] + dx, 
            y: storePosSelectionBlock[1] + dy
        });
       

    }; 

    var pathDragEnd = function() { 
        masterBackground.show();
        $("#draw_container").css({cursor:'default'});
        console.log("move End");
        //console.log(pathStr);
    };

    $('#draw_area').click(function(e) {
        pX = (e.offsetX != null) ? e.offsetX : e.originalEvent.layerX;
        pY = (e.offsetY != null) ? e.offsetY : e.originalEvent.layerY;
        
        console.log("CLICK: "+ pX +'/'+ pY);
        if(mode == 'add') {
            if(shift) {
                var newX = (Math.floor(pX/STEP))*STEP;
                var newY = (Math.floor(pY/STEP))*STEP;
                path = paper.path('M'+newX+','+newY+'L'+newX+','+newY).attr('fill','red');
            }
            else {
                path = paper.path('M'+pX+','+pY+'L'+pX+','+pY).attr('fill','red');            
            }
            mode = 'edit'
            return;
        }

        if(mode == 'edit') {
            pathStr = path.attr('path');
            pathStr.push(['L',pX ,pY]);
            path.attr('path', pathStr);
        }
    });

    $('#draw_area').mousemove(function(e){
        if (mode == 'edit') {
            pX = (e.offsetX != null) ? e.offsetX : e.originalEvent.layerX;
            pY = (e.offsetY != null) ? e.offsetY : e.originalEvent.layerY;
            pathStr = path.attr('path');
            var firstPX = pathStr[pathStr.length-2][1];
            var firstPY = pathStr[pathStr.length-2][2];

            if (ctrl) {
                var R = Math.sqrt(Math.pow((pY - firstPY), 2)+ Math.pow((pX - firstPX), 2));
                var points = [];
                var angle = 0;

                for ( var i = 0; i < 360 / 15; i++) {
                    angle = (15 * i / 180) * Math.PI;
                    x = R * Math.sin(angle) + firstPX;
                    y = R * Math.cos(angle) + firstPY;
                    points[i] = {
                        x : x,
                        y : y
                    };
                }
                var minR = Math.sqrt(Math.pow((points[0].x - pX), 2) + Math.pow((points[0].y - pY), 2));
                minCord = {
                    x : points[0].x,
                    y : points[0].y
                };

                for ( var i = 1; i < points.length; i++) {
                    var temp = Math.sqrt(Math.pow((points[i].x - pX), 2) + Math.pow((points[i].y - pY), 2));
                    if ( temp < minR) {
                        minR = temp;
                        minCord = {
                            x : points[i].x,
                            y : points[i].y
                        };
                    }
                }
                pathStr[pathStr.length-1][1] = minCord.x;
                pathStr[pathStr.length-1][2] = minCord.y;
                path.attr('path', pathStr);
            }
            if (shift) {
                var newX = (Math.floor(pX/STEP))*STEP;
                var newY = (Math.floor(pY/STEP))*STEP;
                console.log(newX, newY);
                pathStr[pathStr.length-1][1] = newX;
                pathStr[pathStr.length-1][2] = newY;
                path.attr('path', pathStr);
            }
            if (!ctrl && !shift){
                pathStr[pathStr.length-1][1] = pX;
                pathStr[pathStr.length-1][2] = pY;
                path.attr('path', pathStr); 
            }
        }
    });   

    $('#draw_area').bind('contextmenu', function(e) {
        e.preventDefault();

        pathStr = path.attr('path');
        pathStr[pathStr.length-1] = ['Z'];
        path.attr('path', pathStr);
        mode = 'add2';
        path.drag(pathDragMove, pathDragStart, pathDragEnd);
        path.click(clickfn);
        path.children = [];

        path.bringToFront = function() {   
            this.toFront();
            this.selectionBlock.toFront();
        }

        path.makeselected = function() {
            this.bringToFront();
            this.selectionBlock.show();
            //this.resizer.show();
            //this.selectionBlock.show();
        }

        path.makeunselected = function() {
            //this.selectionBlock.hide();
        }

        var minX, minY, maxX, maxY;
        minX = pathStr[0][1];
        minY = pathStr[0][2];
        maxX = pathStr[0][1];
        maxY = pathStr[0][2];
        for (var i = 1; i < pathStr.length-1; i++) {
            if (pathStr[i][1] < minX) {
                minX = pathStr[i][1];
            }
            if (pathStr[i][2] < minY) {
                minY = pathStr[i][2];
            }
            if (pathStr[i][1] > maxX) {
                maxX = pathStr[i][1];
            }
            if (pathStr[i][2] > maxY) {
                maxY = pathStr[i][2];
            }            
        };
        console.log(minX +" "+ minY + " " + maxX + " " + maxY);
        path.selectionBlock = add_child(paper.rect(minX-2, minY-2, (maxX-minX)+2, (maxY-minY)+2));
        path.selectionBlock.attr({"stroke": "blue","stroke-width":1});
        path.selectionBlock.attr({'stroke-dasharray': '- '});
        path.selectionBlock.hide();
        path.selectionBlock.drag(pathDragMove, pathDragStart, pathDragEnd);

        console.log("draw end");
        console.log(path);
        console.log(path.selectionBlock);

        masterBackground.show();


        $('#draw_area').unbind('contextmenu');
        $('#draw_area').off();
    });

}

var test = function() {
    var item=0;
    var clickfunction = function() {
        item++;
        console.log("CLICK "+item);
    }
    var rect;

    rect = paper.rect(0,0,200,500);
    rect.attr("fill","red");
    rect.click(clickfunction);
    return rect;

}

function add_test() {
    var obj = new test();
    console.log(obj);
}