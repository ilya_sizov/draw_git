var mod = {};
mod.STEP = 15;
mod.SELECTION_PADDING = 2;
mod.SELECTION_COLOR = '#00F';
mod.MIN_H = 10;
mod.MIN_W = 10;
mod.RESIZER_DIM = 10;

mod.selectNode = function(node) {
    var mainnode   = mod.getMainNode(node);
    paper.forEach(function(el){
        if((el.children) && (mainnode !== el)){
            el.makeunselected();
        }
    }); 
    mainnode.makeselected();
}

mod.storePos = function(node) {
    if(node.type == 'rect') {
        node.ox = node.attr("x");
        node.oy = node.attr("y");
    }
    if(node.type == 'path') {
        var str = node.attr("path");
        node.points = [];
        for (var i = 0; i < str.length-1; i++) {
            node.points.push([str[i][1], str[i][2]]);
        };
    }
}

mod.storeDims = function(node) {
    node.ow = node.attr('width');
    node.oh = node.attr('height');
}

mod.getMainNode = function(node){
    if(!node.children) {
        return node.mainnode;
    }
    else {
        return node;
    }
}