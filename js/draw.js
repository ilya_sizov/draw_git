var paper;
var masterBackground;
var ctrl = false;
var shift = false;



Raphael.el.is_visible = function() {
    return (this.node.style.display !== "none");
}

Raphael.fn.drawGrid = function(x, y, w, h, wv, hv, color) {
    color = color || '#000';

    var rnd = Math.round,
        i,
        modifier = 0,
        path = [
            "M", rnd(x) + modifier, rnd(y) + modifier, 
            "L", rnd(x + w) + modifier, 
            rnd(y) + modifier, 
            rnd(x + w) + modifier, 
            rnd(y + h) + modifier, 
            rnd(x) + modifier, 
            rnd(y + h) + modifier, 
            rnd(x) + modifier, 
            rnd(y) + modifier
        ],
        rowHeight = h / hv,
        columnWidth = w / wv;
    for (i = 1; i < hv; i++) {
        path = path.concat(["M", rnd(x) + modifier, rnd(y + i * rowHeight) + modifier, "H", rnd(x + w) + modifier]);
    }
    for (i = 1; i < wv; i++) {
        path = path.concat(["M", rnd(x + i * columnWidth) + modifier, rnd(y) + modifier, "V", rnd(y + h) + modifier]);
    }
    return this.path(path.join(",")).attr({stroke: color});
};

document.onkeydown = function(event) {
    //console.log(event);
    if (event.keyCode === 17) {
        ctrl = true;
    }
    if (event.keyCode === 16) {
        shift = true;
    }
};

document.onkeyup = function(event) {
    //console.log(event);
    if (event.keyCode === 17) {
        ctrl = false;
    }
    if (event.keyCode === 16) {
        shift = false;
    }
};

$( document ).ready(function() {
    paper = Raphael("draw_area", draw_area.offsetWidth, draw_area.offsetHeight);
    masterBackground = paper.rect(0,0, draw_area.offsetWidth, draw_area.offsetHeight);
    masterBackground.attr("fill", "#fff");
    masterBackground.attr("fill-opacity", 0.2);

    var paperStart = function(event) {
        //console.log("start");
        $("#draw_container").data("scrolTopVal",$("#draw_container").scrollTop());
        $("#draw_container").data("scrolLeftVal",$("#draw_container").scrollLeft());
        if(masterBackground.is_visible()){
            $("#draw_area").css({cursor:'move'});           
        }
    }
    var paperMove = function(dx, dy) {
        // console.log("move");
        $("#draw_container").scrollTop($("#draw_container").data("scrolTopVal")-dy);
        $("#draw_container").scrollLeft($("#draw_container").data("scrolLeftVal")-dx);
    }  
    var paperEnd = function() {
        $("#draw_area").css({cursor:'default'});
    }
    masterBackground.drag(paperMove, paperStart, paperEnd);
});

var draw_objects = function() {
    var pathStr;
    var mainnode;
    var point = paper.circle(0,0,1);
    // console.log(point);
    var pX;
    var pY;
    var mode = 'add';

    masterBackground.hide();
    
    var clickfn = function() {
        mod.selectNode(this);
    };

    var hoverfn = function() {
        $('#draw_area').css('cursor','pointer');
    };

    var hoverOutfn = function() {
        $('#draw_area').css('cursor','default');
    };

    var add_child = function(child) { 
        mainnode.children.push(child); 
        child.mainnode = mainnode; 
        child.hover(hoverfn, hoverOutfn);
        child.click(clickfn);
        return child;
    };

    var pathDragStart = function () {
        masterBackground.hide();   
        mainnode = mod.getMainNode(this);
        mod.storePos(mainnode);
        mod.storePos(mainnode.selectionBlock);
        mod.storePos(mainnode.resizer);
    }; 

    var pathDragMove = function (dx, dy) {
        if (shift) {
            dx = (Math.floor(dx/mod.STEP))*mod.STEP;
            dy = (Math.floor(dy/mod.STEP))*mod.STEP;
        } 
        pathStr = mainnode.attr('path');
        if(shift) {
          for (var i = 0; i < mainnode.points.length; i++) {
              pathStr[i][1] = (Math.floor(mainnode.points[i][0]/mod.STEP))*mod.STEP + dx;
              pathStr[i][2] = (Math.floor(mainnode.points[i][1]/mod.STEP))*mod.STEP + dy;    
          };
          mainnode.attr('path', pathStr);
          var newsbX = (Math.floor((mainnode.selectionBlock.ox+mod.SELECTION_PADDING)/mod.STEP))*mod.STEP-mod.SELECTION_PADDING;
          var newsbY = (Math.floor((mainnode.selectionBlock.oy+mod.SELECTION_PADDING)/mod.STEP))*mod.STEP-mod.SELECTION_PADDING;
          mainnode.selectionBlock.attr({
              x: newsbX + dx, 
              y: newsbY + dy
          });
          var newresX = (Math.floor((mainnode.resizer.ox-mod.SELECTION_PADDING+mod.RESIZER_DIM/2)/mod.STEP))*mod.STEP+mod.SELECTION_PADDING-mod.RESIZER_DIM/2;
          var newresY = (Math.floor((mainnode.resizer.oy-mod.SELECTION_PADDING+mod.RESIZER_DIM/2)/mod.STEP))*mod.STEP+mod.SELECTION_PADDING-mod.RESIZER_DIM/2;
          mainnode.resizer.attr({
              x: newresX + dx, 
              y: newresY + dy
          });          
        }
        else {
          for (var i = 0; i < mainnode.points.length; i++) {
              pathStr[i][1] = mainnode.points[i][0] + dx;
              pathStr[i][2] = mainnode.points[i][1] + dy;           
          };

          mainnode.attr('path', pathStr);
          mainnode.selectionBlock.attr({
              x: mainnode.selectionBlock.ox + dx, 
              y: mainnode.selectionBlock.oy + dy
          });
          mainnode.resizer.attr({
              x: mainnode.resizer.ox + dx,
              y: mainnode.resizer.oy + dy
          });          
        }
    }; 

    var pathDragEnd = function() { 
        masterBackground.show();
        mainnode = mod.getMainNode(this);
    };

    var resizerStart = function() {
        masterBackground.hide();
        mainnode = mod.getMainNode(this);
        mod.storeDims(mainnode.selectionBlock);
        mod.storePos(mainnode.resizer);
        mod.storePos(mainnode);
    };

    var resizerMove = function(dx, dy) {
        if (shift) {
            dx = (Math.floor(dx/mod.STEP))*mod.STEP;
            dy = (Math.floor(dy/mod.STEP))*mod.STEP;
        } 
        var xRes = mainnode.resizer.ox + dx;
        var yRes = mainnode.resizer.oy + dy;
        var wSel = mainnode.selectionBlock.ow + dx;
        var hSel = mainnode.selectionBlock.oh + dy;
        if (wSel <= mod.MIN_W) wSel = mod.MIN_W;
        if (hSel <= mod.MIN_H) hSel = mod.MIN_H;
        if (xRes <= (mainnode.selectionBlock.attr('x') + mod.MIN_W - mod.RESIZER_DIM/2)) xRes = (mainnode.selectionBlock.attr('x') + mod.MIN_W - mod.RESIZER_DIM/2);
        if (yRes <= (mainnode.selectionBlock.attr('y') + mod.MIN_H - mod.RESIZER_DIM/2)) yRes = (mainnode.selectionBlock.attr('y') + mod.MIN_H - mod.RESIZER_DIM/2);
        var factorX = 1 - (wSel-mod.SELECTION_PADDING*2)/(mainnode.selectionBlock.ow-mod.SELECTION_PADDING-2);
        var factorY = 1 - (hSel-mod.SELECTION_PADDING*2)/(mainnode.selectionBlock.oh-mod.SELECTION_PADDING-2);
        pathStr = mainnode.attr('path');

        for (var i = 0; i < mainnode.points.length; i++) {
            var minusierX = (mainnode.points[i][0] - mainnode.selectionBlock.attr('x') - mod.SELECTION_PADDING) * factorX;
            var minusierY = (mainnode.points[i][1] - mainnode.selectionBlock.attr('y') - mod.SELECTION_PADDING) * factorY;
            pathStr[i][1] = mainnode.points[i][0] - minusierX;
            pathStr[i][2] = mainnode.points[i][1] - minusierY;
        };

        mainnode.selectionBlock.attr({ 
            width: wSel,
            height: hSel
        });
        mainnode.resizer.attr({
            x: xRes,
            y: yRes
        });
        mainnode.attr('path', pathStr);
    };

    var resizerEnd = function() {
        masterBackground.show();
    };

    $('#draw_area').click(function(e) {
        pX = (e.offsetX != null) ? e.offsetX : e.originalEvent.layerX;
        pY = (e.offsetY != null) ? e.offsetY : e.originalEvent.layerY;
        
        console.log("CLICK: "+ pX +'/'+ pY);
        if(mode == 'add') {
            if(shift) {
                var newX = (Math.floor(pX/mod.STEP))*mod.STEP;
                var newY = (Math.floor(pY/mod.STEP))*mod.STEP;
                mainnode = paper.path('M'+newX+','+newY+'L'+newX+','+newY).attr('fill','#5F6D7D');
            }
            else {
                mainnode = paper.path('M'+pX+','+pY+'L'+pX+','+pY).attr('fill','#5F6D7D');            
            }
            mode = 'edit'
            return;
        }

        if(mode == 'edit') {
            pathStr = mainnode.attr('path');
            pathStr.push(['L',pX ,pY]);
            mainnode.attr('path', pathStr);
        }
    });

    $('#draw_area').mousemove(function(e){
        pX = (e.offsetX != null) ? e.offsetX : e.originalEvent.layerX;
        pY = (e.offsetY != null) ? e.offsetY : e.originalEvent.layerY;

        if (shift) {
            var newX = (Math.floor(pX/mod.STEP))*mod.STEP;
            var newY = (Math.floor(pY/mod.STEP))*mod.STEP;
            pX = newX;
            pY = newY;
        }

                
        if(mode == 'add') {
            point.attr({ cx: pX, cy: pY, r:5});
        }

        if (mode == 'edit') {
            point.attr({ cx: pX, cy: pY, r:5});
            pathStr = mainnode.attr('path');
            var firstPX = pathStr[pathStr.length-2][1];
            var firstPY = pathStr[pathStr.length-2][2];

            if (ctrl) {
                var R = Math.sqrt(Math.pow((pY - firstPY), 2)+ Math.pow((pX - firstPX), 2));
                var points = [];
                var angle = 0;

                for ( var i = 0; i < 360 / 15; i++) {
                    angle = (15 * i / 180) * Math.PI;
                    x = R * Math.sin(angle) + firstPX;
                    y = R * Math.cos(angle) + firstPY;
                    points[i] = {
                        x : x,
                        y : y
                    };
                }
                var minR = Math.sqrt(Math.pow((points[0].x - pX), 2) + Math.pow((points[0].y - pY), 2));
                minCord = {
                    x : points[0].x,
                    y : points[0].y
                };

                for ( var i = 1; i < points.length; i++) {
                    var temp = Math.sqrt(Math.pow((points[i].x - pX), 2) + Math.pow((points[i].y - pY), 2));
                    if ( temp < minR) {
                        minR = temp;
                        minCord = {
                            x : points[i].x,
                            y : points[i].y
                        };
                    }
                }
                pathStr[pathStr.length-1][1] = minCord.x;
                pathStr[pathStr.length-1][2] = minCord.y;
                mainnode.attr('path', pathStr);
            }
            else {
                var newX = (Math.floor(pX/mod.STEP))*mod.STEP;
                var newY = (Math.floor(pY/mod.STEP))*mod.STEP;
                pathStr[pathStr.length-1][1] = pX;
                pathStr[pathStr.length-1][2] = pY;
                mainnode.attr('path', pathStr); 
            }
        }
    });   

    $('#draw_area').bind('contextmenu', function(e) {
        e.preventDefault();
        point.remove();
        pathStr = mainnode.attr('path');
        pathStr[pathStr.length-1] = ['Z'];
        mainnode.attr('path', pathStr);
        mode = 'add2';
        mainnode.drag(pathDragMove, pathDragStart, pathDragEnd);
        mainnode.click(clickfn);
        mainnode.hover(hoverfn, hoverOutfn);
        mainnode.children = [];
        var box = mainnode.getBBox();
        mainnode.selectionBlock = add_child(paper.rect(box.x-mod.SELECTION_PADDING, box.y-mod.SELECTION_PADDING, box.width+mod.SELECTION_PADDING*2, box.height+mod.SELECTION_PADDING*2)); 
        mainnode.selectionBlock.attr({"stroke": mod.SELECTION_COLOR,"stroke-width":1});
        mainnode.selectionBlock.attr({'stroke-dasharray': '- '});
        mainnode.selectionBlock.hide();
        mainnode.selectionBlock.hover(hoverfn, hoverOutfn);
        mainnode.selectionBlock.drag(pathDragMove, pathDragStart, pathDragEnd);

        mainnode.resizer = add_child(paper.rect(box.x2+mod.SELECTION_PADDING-mod.RESIZER_DIM/2, box.y2+mod.SELECTION_PADDING-mod.RESIZER_DIM/2, mod.RESIZER_DIM, mod.RESIZER_DIM).attr({"fill": "black"}));
        mainnode.resizer.hide();
        mainnode.resizer.hover(function() {$('#draw_area').css('cursor','nw-resize');}, function() {$('#draw_area').css('cursor','default');});
        mainnode.resizer.drag(resizerMove, resizerStart, resizerEnd);
        masterBackground.show();

        mainnode.bringToFront = function(safe) {
            this.toFront();
            this.selectionBlock.toFront();
            this.resizer.toFront();
        }

        mainnode.makeselected = function() {
          if(!ctrl) {
            this.selected=true;
            this.bringToFront();
            this.selectionBlock.show();
            this.resizer.show();
          }
          else {
            this.selected=true;
          }
        }

        mainnode.makeunselected = function() {
          if(!ctrl) {
            this.selected=false;
            this.selectionBlock.hide();
            this.resizer.hide();
          }
          else {
            this.selectionBlock.hide();
            this.resizer.hide();
          }
        }
        $('#draw_area').unbind('contextmenu');
    });

    return mainnode;
}

function add_new_path() {
    var path = new draw_objects();
}

function grid_show(){
        var grid = paper.drawGrid(0,0,draw_area.offsetWidth,draw_area.offsetHeight,draw_area.offsetWidth/mod.STEP,draw_area.offsetHeight/mod.STEP,'#C0C0C0');
        grid.name = "grid";
        $('#btnGridShow')[0].disabled = true;
        //console.log($('#grid_show')); 
}

function grid_hide() {
    paper.forEach(function(el){
        if((!el.children) && (el.name == 'grid')){
            el.remove();
        }
    });
    $('#btnGridShow')[0].disabled = false;
}

function  select_group() {
  var mode = 'add';
  var selectBox;
  var hoverfn = function() {
    $('#draw_area').css('cursor','pointer');
  };

  var hoverOutfn = function() {
    $('#draw_area').css('cursor','default');
  };

  var sDragStart = function () {
    masterBackground.hide();
    mod.storePos(selectBox); 
    console.log('start');  
  }; 

  var sDragMove = function (dx, dy) {
    if (shift) {
        dx = (Math.floor(dx/mod.STEP))*mod.STEP;
        dy = (Math.floor(dy/mod.STEP))*mod.STEP;
    }

    selectBox.attr({
        x: selectBox.ox + dx, 
        y: selectBox.oy + dy
    });
  }; 

  var sDragEnd = function() { 
    masterBackground.show();
    console.log('end'); 
  };


  $('#draw_area').mousedown(function(e) {
    if (mode == 'add') {
      pX = (e.offsetX != null) ? e.offsetX : e.originalEvent.layerX;
      pY = (e.offsetY != null) ? e.offsetY : e.originalEvent.layerY;
      selectBox = paper.rect(pX, pY, 1,1);
      selectBox.attr({"stroke": mod.SELECTION_COLOR,"stroke-width":1});
      selectBox.attr({'stroke-dasharray': '- '});
      selectBox.attr('fill', '#fff');
      selectBox.attr("fill-opacity", 0.2);
      console.log('CLICK' + selectBox);
      mode = 'edit';
    }
  });

  $('#draw_area').mousemove(function(e) {
    pX = (e.offsetX != null) ? e.offsetX : e.originalEvent.layerX;
    pY = (e.offsetY != null) ? e.offsetY : e.originalEvent.layerY;
    if(mode == 'edit')  {
      console.log('mousemove');
      var sX = selectBox.attr('x');
      var sY = selectBox.attr('y');
      selectBox.attr({
        width: pX - sX,
        height: pY - sY
      });
    }
  });

  $('#draw_area').mouseup(function(e) {
    mode='end';
    selectBox.drag(sDragMove, sDragStart, sDragEnd);
    selectBox.hover(hoverfn, hoverOutfn);
  });
}

$('#test').change(function() {
  console.log('123');
});